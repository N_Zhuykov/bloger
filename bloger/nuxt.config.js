export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'bloger',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
      {
        name: 'format-detection',
        content: 'telephone=no'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css',
    '@/assets/styles/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
   { src: '~plugins/nuxt-quill-plugin', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/auth-next',
  ],

  axios: {
    proxy: true // Can be also an object with default options
  },

  proxy: {
    // '/api/v1': { target: 'http://api.contac1m.beget.tech/', pathRewrite: {'^/api/v1': ''} },
    '/api/v2': { target: 'http://localhost:5000/', pathRewrite: {'^/api/v2': ''} }
  },

  auth: {
    localStorage: false,
    strategies: {
      local: {
        token: {
          property: 'token',
          global: true,
          type: 'Bearer'
        },
        user:{
          property: 'user'
        },
        endpoints: {
          login: { url: '/api/v1/authorization', method: 'post'},
          // login: { url: '/api/v3/authorization_admin', method: 'get'},
          logout: false,
          user: false,
        }
      }
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
